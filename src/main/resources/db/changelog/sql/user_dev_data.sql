INSERT INTO app_user (username, password, status, created_by, created_time, modified_by, modified_time)
VALUES
('user1', '$2a$04$4owfw3qhKPat6tTusc4upu/MtGkkCaE5uZ7GqptLhdANRFC/FrbAC', 'A', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP),
('user2', '$2a$04$z0MITJGulTEZLkBWuMkKuOOt4gbcxVykTXq7TH.fnr9oEFzrPm5x2', 'A', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP),
('user3', '$2a$04$asE8wTTBF9SwmVQuGBmRCOPWMds71rkrzA3c.IAjnC9gkE9IDAh8G', 'A', 'admin', CURRENT_TIMESTAMP, 'admin', CURRENT_TIMESTAMP);
